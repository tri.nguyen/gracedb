# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-01-16 20:07:23
from __future__ import unicode_literals

from django.db import migrations

# List of label names, default colors, and descriptions
LABELS = [
    {
        'name': 'HWINJREQ',
        'defaultColor': 'black',
        'description': 'A hardware injection is scheduled.',
    },
    {
        'name': 'HWINJOK',
        'defaultColor': 'green',
        'description': 'A hardware injection was successfully performed.',
    },
    {
        'name': 'HWINJNO',
        'defaultColor': 'red',
        'description': 'There were problems with the hardware injection.',
    },
]

def add_labels(apps, schema_editor):
    Label = apps.get_model('events', 'Label')

    # Create labels
    for label_dict in LABELS:
        l, created = Label.objects.get_or_create(name=label_dict['name'])
        l.defaultColor = label_dict['defaultColor']
        l.description = label_dict['description']
        l.save()


def remove_labels(apps, schema_editor):
    Label = apps.get_model('events', 'Label')

    # Delete labels
    for label_dict in LABELS:
        try:
            l = Label.objects.get(name=label_dict['name'])
        except Label.DoesNotExist:
            print('Label {0} not found to be deleted, skipping.' \
                .format(label_dict['name']))
            break
        l.delete()


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0030_siminspiral_source_and_destination_channels_null'),
    ]

    operations = [
        migrations.RunPython(add_labels, remove_labels),
    ]
