# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-25 19:16
from __future__ import unicode_literals

from django.db import migrations

# Creates initial search pipeline instances

# List of search pipeline names
SEARCH_PIPELINES = [
    'MBTAOnline',
    'CWB2G',
    'gstlal',
    'gstlal-spiir',
    'HardwareInjection',
    'X',
    'Q',
    'Omega',
    'Ringdown',
    'Fermi',
    'Swift',
    'CWB',
    'SNEWS',
    'LIB',
    'pycbc',
]

def add_pipelines(apps, schema_editor):
    Pipeline = apps.get_model('events', 'Pipeline')

    # Create pipelines
    for pipeline_name in SEARCH_PIPELINES:
        pipeline, created = Pipeline.objects.get_or_create(name=pipeline_name)

def remove_pipelines(apps, schema_editor):
    Pipeline = apps.get_model('events', 'Pipeline')

    # Delete pipelines
    Pipeline.objects.filter(name__in=SEARCH_PIPELINES).delete()

class Migration(migrations.Migration):

    dependencies = [
        ('events', '0002_initial_group_data'),
    ]

    operations = [
        migrations.RunPython(add_pipelines, remove_pipelines),
    ]
