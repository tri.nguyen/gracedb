.. GraceDB documentation master file, created by
   sphinx-quickstart on Mon Jun 15 09:22:37 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to GraceDB's documentation!
===================================

Contents:

.. toctree::
   :maxdepth: 2

   ref_manual 
   tutorials
   LIGO/Virgo Public Alert Guide <https://emfollow.docs.ligo.org/userguide/>
   Report a bug (LIGO/Virgo users) <https://git.ligo.org/lscsoft/gracedb/issues>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

